## Lab Outline.

This lab uses two virtual machines, an attacker and a victim. The victim is running a vulnerable Docker install. 

The attacker will leverage an OS Command injection vulnerability to launch malware on the victim. 

**Learn more about this techniques at:**
https://cybr.com/courses/introduction-to-os-command-injections/

