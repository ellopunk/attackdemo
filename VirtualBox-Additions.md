# Installing Guest Edition Virtual Box

## Requirments

Insert Guest Additions CD Image
Updated distro install

## Install
 
- **sudo apt install build-essential**
- **sudo apt install build-essential dkms linux-headers-$(uname -r)**

If you receive the error *Unable to locate package dmks*

- **sudo apt install build-essential**

Then try **sudo apt install build-essential dkms Linux-headers-$(uname -r)** again
    
- **sudo mkdir -p /mnt/addition**

- **df-h**

You are looking for where the VGuest Edition disk is mounted. Mine was sdr1.

- **sudo mount /dev/sr1 /mnt/addition**
- **cd /mnt/addition**
- **sudo sh ./VBoxLinuxAdditions.run --nox11**

Reboot your box, and congratulations, you can now use your VM in full screen!  Don't forget to eject the Guest Editions disk. 
