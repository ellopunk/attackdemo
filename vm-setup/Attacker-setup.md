# Ubuntu Attacker Setup

## Install Comimix

* $ sudo apt update
* $ sudo apt upgrade
* $ sudo apt install python2 <br>
 ***python3 will not work in this lab***
* $ wget https://github.com/commixproject/commix/archive/master.zip
* $ unzip master.zip
* $ rm master.zip <br>
  ***Do not skip this step. It will help avoid confusion later on.***
* $ cd commix-master
* $ python2 commix.py
* $ open browser and go to http://<>:3000/commix-testbed/ <br>
***https will not work** 

## Install Weevely

***Make sure that the python package manager and yaml libraries are installed***

* $ wget https://github.com/epinna/weevely3/archive/master.zip
* $ unzip master.zip
* $ rm master.zip
* $ sudo apt install pip
* $ cd weevely3-master/
* $ sudo pip3 install -r requirements.txt --upgrade
* $ ./weevely.py generate commix agent.php
* $ weevely generate commix weevely.php
