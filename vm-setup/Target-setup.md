# Target VM Setup

* $ sudo apt update
* $ sudo apt upgrade
* $ sudo apt install -y docker.io
* $ sudo systemctl enable docker --now
* $ sudo usermod -aG docker $USER
* $ newgrp docker
* $ docker run --rm -d -p 3000:80 cybrcom/commix-testbed <br>
Look at http://localhost:3000/commix-testbed
* $ docker ps
* $ docker exec -it <container #> chown -R www-data:www-data    commix-testbed
